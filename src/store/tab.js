export default {
    state: {
        isCollapse: false,
        tabsList: [
            {
                path: "/",
                name: "home",
                label: "首页",
                icon: "s-home",
                url: "Home/Home"
            }
        ]
    },
    mutations: {
        collapseMenu(state) {
            state.isCollapse = !state.isCollapse
        },
        selectMenu(state, val){
            if(val.name !== 'home'){
                //findIndex 数组操作方法 判断括号内的逻辑 存在这返回item 不存在返回 -1
                const index = state.tabsList.findIndex( item => item.name === val.name)
                if( index === -1){
                    state.tabsList.push(val)
                }
            }

        }
    }
}