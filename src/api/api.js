import api from '../utils/request'

// 请求用户数据接口
export const getData = () => {
    //返回一个promise对象
    return api.get('/user/getData')
}
export const login = (data) => {
    //返回一个promise对象
    console.log(data)
    return api.post('/login',{data:data})
}